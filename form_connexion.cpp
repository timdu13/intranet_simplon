#include "form_connexion.h"
#include "ui_form_connexion.h"

#include <QString>
#include <QtSql>

#include <iostream>

using namespace std;

Form_connexion::Form_connexion(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Form_connexion)
{
    ui->setupUi(this);

    QObject::connect( this->ui->inscription_button, SIGNAL( clicked() ), this->parentWidget(), SLOT( goInscription() ) );
    QObject::connect( this->ui->valider_button, SIGNAL( clicked() ), this, SLOT( try_to_connect() ) );
    QObject::connect( this, SIGNAL( s_done_connexion( int ) ), this->parentWidget(), SLOT( doneConnexion( int ) ) );
}

Form_connexion::~Form_connexion()
{
    delete ui;
}

void Form_connexion::try_to_connect() {
    if (this->ui->mdp->text().count() == 0) return;

    this->ui->valider_button->setText( "En attente..." );

    QString email, mdp;
    email = this->ui->email->text();
    mdp = this->ui->mdp->text();

    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");

    db.setHostName( "localhost" );
    db.setUserName( "dev-cpp" );
    db.setPassword( "Djn64jb.cpp!" );
    db.setDatabaseName( "intranet_simplon" );

    if( db.open() ) {
        cout << "Vous êtes maintenant connecté à " << db.hostName().toStdString() << endl;
    }
    else {
        // @todo: Gérer l'affichage graphique de l'erreur
        cout << "### ERREUR, IMPOSSIBLE A SE CONNECTER A LA BASE DE DONNEES ###";
        return;
    }

    QSqlQuery query;
    query.prepare( "SELECT ID, mdp FROM user WHERE email=:email" );
    query.bindValue( ":email", email );

    if( query.exec() ) {
        query.next();
        if( query.value(1).toString() == mdp ){
            cout << "########## CA MARCHE" << endl;
            emit s_done_connexion( query.value(0).toInt() );
        }
        else {
            this->ui->valider_button->setText( "Valider" );
        }
    }
    else {
        this->ui->valider_button->setText( "Valider" );
    }


























}

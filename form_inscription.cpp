#include "form_inscription.h"
#include "ui_form_inscription.h"

#include "validation.h"

#include <sstream> // Pour la conversion d'int en string

#include <QDebug>
#include <QString>
#include <QLabel>
#include <QHBoxLayout>
#include <QRegExp>

#include <QtSql>

#include <QtUiTools/QUiLoader>
#include <QStringList>

#include <iostream>
#include <algorithm>
#include "string"
Form_inscription::Form_inscription(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Form_inscription)
{
    ui->setupUi(this);

    this->nom_valide    = false;
    this->prenom_valide = false;
    this->email_valide  = false;
    this->mdp_valide    = false;
    this->mdp2_valide   = false;

    // Connexion des signaux et des slots pour les bouttons
    QObject::connect( ui->renit_button,     SIGNAL( clicked( ) ), this, SLOT( onClickRenit( ) ) );
    QObject::connect( ui->valider_button,   SIGNAL( clicked( ) ), this, SLOT( onClickValider( ) ) );
    QObject::connect( ui->annuler_button,   SIGNAL( clicked( ) ), this, SLOT( onClickAnnuler( ) ) );

    QObject::connect( this, SIGNAL( doneInscription() ), this->parentWidget(), SLOT( goConnection() ) );


    // Connexion des signaux et des slots pour les validations d'input
    // Pour chaque champ, une connexion pour le textEdited et une pour le click sur valider (déclencher manuellement dans le slot onClickValider)
    QObject::connect( ui->prenom_champ,     SIGNAL( textEdited( QString ) ),    this, SLOT( validePrenom( QString ) ) );
    QObject::connect( this,                 SIGNAL( clickInscription( ) ),      this, SLOT( validePrenom( QString ) ) );

    QObject::connect( ui->nom_champ,        SIGNAL( textEdited( QString ) ),    this, SLOT( valideNom( QString ) ) );
    QObject::connect( this,                 SIGNAL( clickInscription( ) ),      this, SLOT( valideNom( QString ) ) );

    QObject::connect( ui->email_champ,      SIGNAL( textEdited( QString ) ),    this, SLOT( valideEmail( QString ) ) );
    QObject::connect( this,                 SIGNAL( clickInscription( ) ),      this, SLOT( valideEmail( QString ) ) );

    QObject::connect( ui->mdp_champ,        SIGNAL( textEdited( QString ) ),    this, SLOT( valideMdp( QString ) ) );
    QObject::connect( this,                 SIGNAL( clickInscription( ) ),      this, SLOT( valideMdp( QString ) ) );

    QObject::connect( ui->mdp2_champ,       SIGNAL( textEdited( QString ) ),    this, SLOT( valideMdp2( QString ) ) );
    QObject::connect( this,                 SIGNAL( clickInscription( ) ),      this, SLOT( valideMdp2( QString ) ) );
}

Form_inscription::~Form_inscription()
{
    delete ui;
}

void Form_inscription::add_error_form( map<int, bool> error, string nomChamp ) {
    // Ajoute n entrée dans this->error_form

    // On commence par supprimer toutes les anciennes error_form du champ
    this->clear_error_champ( nomChamp );

    // Pour chaque error, on remplis le tableau error_form
    map<int, bool>::iterator it;
    for ( it = error.begin() ; it != error.end() ; ++it ) {
        if ( it->second == true ) {
            this->error_form[ nomChamp ].push_back( it->first );
        }
    }
}
void Form_inscription::add_error_form( int code_error, string nomChamp ) {
    this->error_form[ nomChamp ].push_back( code_error );
}

void Form_inscription::reset_zone_msg( void ) {

    map<string, vector<int> >::iterator it_error_form;

    vector<int> error_champ;
    vector<int>::iterator it_error_champ;

    int nb_error = 0;
    int nb_champ_faux = 0;

    QString nouveauMsg;
    // On itère sur la map qui contient toutes les erreurs du form
    for ( it_error_form = this->error_form.begin() ; it_error_form != this->error_form.end() ; ++it_error_form ) {
        error_champ = it_error_form->second;
        nb_champ_faux++;

        // On itère sur les erreurs d'un champ
        for ( it_error_champ = error_champ.begin() ; it_error_champ != error_champ.end() ; ++it_error_champ ) {
            nouveauMsg += this->decodeError( it_error_form->first, *it_error_champ );
            nb_error++;
        }
    }
    this->ui->zone_msg->setText( nouveauMsg );

    // On met à jour l'indicateur de présence d'erreur(s)
    if ( nb_error > 0 )
        this->hasError = true;
    else
        this->hasError = false;

    // On met à jour la progressbar
    int valPB = 0;
    if ( this->nom_valide == true ) valPB += 20;
    if ( this->prenom_valide == true ) valPB += 20;
    if ( this->email_valide == true ) valPB += 20;
    if ( this->mdp_valide == true ) valPB += 20;
    if ( this->mdp2_valide == true ) valPB += 20;
    this->ui->progressBar->setValue( valPB );
}

QString Form_inscription::decodeError( string nomChamp, int codeError ) {
    // Convert int to string
    ostringstream convert;
    convert << codeError;

    string msg = "";


    msg += "\nERREUR #";
    msg += convert.str();
    msg += " sur le champ '";
    msg += nomChamp;

    QString r = QString::fromStdString( msg );

    return  r;
}

void Form_inscription::clear_error_champ( string nomChamp ) {
    map<string, vector<int> >::iterator if_exist = this->error_form.find( nomChamp );

    if( if_exist != this->error_form.end() ) {
        // Si il existe on le vide
        if_exist->second.clear();
    }
}

/* SLOTS */
void Form_inscription::onClickRenit( ) {
    this->clear_error_champ( "prenom" );
    this->clear_error_champ( "nom" );
    this->clear_error_champ( "email" );
    this->clear_error_champ( "mdp" );
    this->clear_error_champ( "mdp2" );

    this->reset_zone_msg();
}

void Form_inscription::valideNom( QString value ) {
    //*
    if( value.isEmpty() ) {
        this->clear_error_champ( "nom" );
        this->reset_zone_msg();
        return;
    }

    Validation val( value );

    val.addTest( T_ONLY_LETTERS );
    val.addTest( T_LENGHT_MIN, 2 );
    val.addTest( T_LENGHT_MAX, 30 );

    val.exeTest();

    map<int, bool> error = val.getError();

    this->add_error_form( error, "nom" );
    this->nom_valide = this->maj_nb_champ_faux( error );
    this->reset_zone_msg();

    //*/
}


void Form_inscription::validePrenom( QString value ) {
    if( value.isEmpty() ) {
        this->clear_error_champ( "prenom" );
        this->reset_zone_msg();
        return;
    }

    Validation val( value );

    val.addTest( T_ONLY_LETTERS );
    val.addTest( T_LENGHT_MIN, 2 );
    val.addTest( T_LENGHT_MAX, 30 );

    val.exeTest();

    map<int, bool> error = val.getError();

    this->add_error_form( error, "prenom" );
    this->prenom_valide = this->maj_nb_champ_faux( error );
    this->reset_zone_msg();

}
void Form_inscription::valideEmail( QString value ) {
    if( value.isEmpty() ) {
        this->clear_error_champ( "email" );
        this->reset_zone_msg();
        return;
    }

    Validation val( value );

    val.addTest( T_EMAIL_FORMAT );
    val.addTest( T_LENGHT_MAX, 255 );

    val.exeTest();

    map<int, bool> error = val.getError();

    this->add_error_form( error, "email" );
    this->email_valide = this->maj_nb_champ_faux( error );
    this->reset_zone_msg();

}
void Form_inscription::valideMdp( QString value ) {
    if( value.isEmpty() ) {
        this->clear_error_champ( "mdp" );
        this->reset_zone_msg();
        return;
    }

    Validation val( value );

   // val.addTest( T_AT_LEAST_CAPITAL );
    //val.addTest( T_AT_LEAST_NUMBER );
    val.addTest( T_LENGHT_MIN, 8 );
    val.addTest( T_LENGHT_MAX, 30 );

    val.exeTest();

    map<int, bool> error = val.getError();

    this->add_error_form( error, "mdp" );
    this->mdp_valide = this->maj_nb_champ_faux( error );
    this->reset_zone_msg();

}
void Form_inscription::valideMdp2( QString value ) {
    if( value.isEmpty() ) {
        this->clear_error_champ( "mdp2" );
        this->reset_zone_msg();
        return;
    }

    Validation val( value );

    val.addTest( T_EQUALITY, this->ui->mdp_champ->text() );

    val.exeTest();

    map<int, bool> error = val.getError();

    this->add_error_form( error, "mdp2" );
    this->mdp2_valide = this->maj_nb_champ_faux( error );
    this->reset_zone_msg();

}

void Form_inscription::onClickValider( void ) {
    // On lance les tests et si il y a une erreur on fait rien...
    emit clickInscription();
    if ( this->hasError )
        return;

    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName( "localhost" );
    db.setUserName( "dev-cpp" );
    db.setPassword( "Djn64jb.cpp!" );
    db.setDatabaseName( "intranet_simplon" );

    if( db.open() ) {
        cout << "Vous êtes maintenant connecté à " << db.hostName().toStdString() << endl;
    }
    else {
        // @todo: Gérer l'affichage graphique de l'erreur
        cout << "### ERREUR, IMPOSSIBLE A SE CONNECTER A LA BASE DE DONNEES ###";
        return;
    }

    QSqlQuery query;
    query.prepare( "INSERT INTO user (nom, prenom, email, mdp) VALUES (:nom, :prenom, :email, :mdp)" );
    query.bindValue( ":nom", this->ui->nom_champ->text() );
    query.bindValue( ":prenom", this->ui->prenom_champ->text() );
    query.bindValue( ":email", this->ui->email_champ->text() );
    query.bindValue( ":mdp", this->ui->mdp_champ->text() );


    if (query.exec()) {
        // @todo: Gérer l'affichage graphique du succes
        cout << "Ça marche ! :)" << std::endl;
        emit doneInscription();
    }
    else {
        // @todo: Gérer l'affichage graphique de l'erreur
        cout << "Ça marche pas ! :(" << std::endl;
    }
}

void Form_inscription::onClickAnnuler( void ) {
    QObject *papa = this->parent();
}

bool Form_inscription::maj_nb_champ_faux ( map<int, bool> error ){
    map<int, bool>::iterator i_error;
    bool champ_juste = true;


    for( i_error = error.begin() ; i_error != error.end() ; ++i_error ) {
        if ( i_error->second == true ) {
            champ_juste = false;
        }
    }

    return champ_juste;
}

















#ifndef FORM_INSCRIPTION_H
#define FORM_INSCRIPTION_H

#include <QDialog>

#include <QString>

#include <vector>
#include <map>
#include <string>
using namespace std;
namespace Ui {
class Form_inscription;
}

class Form_inscription : public QDialog
{
    Q_OBJECT

public:
    explicit Form_inscription(QWidget *parent = 0);
    ~Form_inscription();

    bool hasError;
    int nb_champ_faux;

    bool nom_valide;
    bool prenom_valide;
    bool email_valide;
    bool mdp_valide;
    bool mdp2_valide;



signals:
    void clickInscription( );
    void doneInscription( );

public slots:
    void onClickRenit( );
    void onClickValider( );
    void onClickAnnuler( );


    void valideNom( QString value );
    void validePrenom( QString value );
    void valideEmail( QString value );
    void valideMdp( QString value );
    void valideMdp2( QString value );


private:
    Ui::Form_inscription *ui;

    map<string, vector<int> > error_form;

    void add_error_form( map<int, bool> error, string nomChamp );
    void add_error_form( int code_error, string nomChamp );

    void reset_zone_msg( void );
    void clear_error_champ( string nomChamp );
    QString decodeError( string nomChamp, int codeError );

    bool maj_nb_champ_faux( map<int, bool> error );
};

#endif // FORM_INSCRIPTION_H

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QString>

#include "form_inscription.h"
#include "form_connexion.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    Form_connexion *connexion;
    Form_inscription *inscription;

public slots:
    void goInscription( void );
    void goConnection( void );
    void doneConnexion( int ID );

private:
    Ui::MainWindow *ui;

    void init (int ID);
};

#endif // MAINWINDOW_H

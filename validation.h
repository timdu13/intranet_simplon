#ifndef VALIDATION_H
#define VALIDATION_H

#include <string>
#include <QString>
#include <map>
#include <vector>
using namespace std;


/**
 * Constantes des codes tests
 */
const int T_ONLY_LETTERS            = 1001;
const int T_ONLY_LETTERS_OR_NUMBERS = 1002;
const int T_LENGHT_MIN              = 1003;
const int T_LENGHT_MAX              = 1004;
const int T_EMAIL_FORMAT            = 1005;
const int T_AT_LEAST_CAPITAL        = 1006;
const int T_AT_LEAST_NUMBER         = 1007;
const int T_EQUALITY                = 1008;

const string REG_ONLY_LETTERS       = "^[A-Za-zéèçàêë-]+$";

const int TEST_MAX                  = 5;
const int ARG_MAX                   = 1;

class Validation {
    public:
        Validation( void );
        Validation( QString value );

        void addTest( int code_test, int arg = 0 );
        void addTest( int code_test, QString arg);

        void exeTest( void );
        void setValue( QString value );
        map<int, bool> getError( void );

    private:
        /**
         * Valeur sur laquelle va s'appliquer les tests
         * @brief value
         */
        QString value;

        /**
         * Liste des tests à faire (Contient les code tests (voir plus haut))
         * @brief test_todo
         */
        map<int, int> test_todo;


        /**
         * Erreur retourné par les tests (Contient les code tests (voir plus haut))
         * @brief code_error
         */
        map<int, bool> code_error;

        /* METHODE */
        void success( int codeTest );
        void error( int codeTest );

        void only_letters( void );
        void only_letters_or_numbers( void );
        void lenght_min( int lenghtMin );
        void lenght_max( int lenghtMax );
        void email_format( void );
        void at_least_capital( void );
        void at_least_number( void );
        void equality ( QString val );

};

#endif // VALIDATION_H
